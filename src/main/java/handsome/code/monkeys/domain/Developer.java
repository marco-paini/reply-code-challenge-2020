package handsome.code.monkeys.domain;

import java.util.Set;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@PlanningEntity
@RequiredArgsConstructor
public class Developer {
	@Max(30)
	@Min(1)
	private final int B;

	private final String C;

	@PlanningVariable(nullable = true, valueRangeProviderRefs = "dDesks")
	private Desk desk;

	@PlanningId
	private final int i;

	@Size(max = 100, min = 1)
	private final Set<String> S;
}
